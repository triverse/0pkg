#!/execute/sh
# Copyright (c) 2015-2018 Ali H. Caliskan <ali.h.caliskan@gmail.com>

. lib/libpath.sh
. $cfgdir/0pkg
. $libdir/lib0pkg

install_packages() {
    if [ -f $RCP_FILE ]; then
        . $RCP_FILE
        if [ ! -z "$BAK" ]; then backup_files ${BAK[@]}; unset BAK; fi

        if [ -f "$ARC_PKG_DIR/${PKG}_${VER}_${PKG_EXT}" ]; then
            printf_green "installing" "$PKG-$VER"
            TAR_OPT="--keep-directory-symlink --no-overwrite-dir -Ipixz"
            tar -C $ROOTDIR $TAR_OPT -xpf $ARC_PKG_DIR/${PKG}_${VER}_${PKG_EXT}
        else
            printf_red "error" "${PKG}_${VER}_${PKG_EXT}: no such file"
        fi

        unset PKG VER
    else
        if [ -z "$GRP_LST" ]; then
            printf_red "error" "$i: no such recipe"
        fi
    fi
}

if [ $# -eq 0 ]; then echo "try $(basename $0) <package>"; exit 1; fi

arguments=($@)

for i in ${!arguments[@]}; do
    case "${arguments[$i]}" in
        rootdir=*)
            ROOTDIR=${arguments[$i]#*=}
            unset arguments[$i]
            ;;
        *)
            if [ -d $PKG_RCP_DIR/${arguments[$i]} ]; then
                GRP_LST+="$(find $PKG_RCP_DIR/${arguments[$i]} -maxdepth 2 -name RECIPE) "
                unset arguments[$i]
            fi
            ;;
    esac
done

if [ -n "$GRP_LST" ]; then
    GRP_LST=$(for i in $GRP_LST; do echo $i; done | sort)
    arguments+=('grp_pkgs')
fi

for i in ${arguments[@]}; do
    case "$i" in
        grp_pkgs)
            for _pkg in $GRP_LST; do
                RCP_FILE=$_pkg
                install_packages
            done
            ;;
        *)
            RCP_FILE=$(find $PKG_RCP_DIR/ -maxdepth 2 -name $i)/RECIPE
            install_packages
            ;;
    esac
done
for i in ${arguments[@]}; do
    case "$i" in
        grp_pkgs)
            for _pkg in $GRP_LST; do
                . $_pkg
                package_setup P_ADD
                package_hooks
                unset PKG VER
            done
            ;;
        *)
            RCP_FILE=$(find $PKG_RCP_DIR/ -maxdepth 2 -name $i)/RECIPE
            if [ -f "$RCP_FILE" ]; then
                . $RCP_FILE
                package_setup P_ADD
                package_hooks
                unset PKG VER
            fi
            ;;
    esac
done

