#!/execute/sh
# Copyright (c) 2015-2018 Ali H. Caliskan <ali.h.caliskan@gmail.com>

. lib/libpath.sh
. $cfgdir/0pkg
. $libdir/lib0pkg

update_packages() {
    package_setup A_UPD

    if [ ! -z "$BAK" ]; then backup_files ${BAK[@]}; unset BAK; fi

    RN=$ROOTDIR/$PKG_LST_DIR/$PKG; cp $RN $RN.bak
    if [ -f "$ARC_PKG_DIR/${PKG}_${RCP_VER}_${PKG_EXT}" ]; then
        printf_green "updating" "$PKG ($INF_VER -> $RCP_VER)"
        TAR_OPT="--keep-directory-symlink --no-overwrite-dir -Ipixz"
        tar -C $ROOTDIR $TAR_OPT -xpf $ARC_PKG_DIR/${PKG}_${RCP_VER}_${PKG_EXT}

        TMP_FILE=$(mktemp $ROOTDIR/$vardir/tmp/0pkg.XXXXXXXXXX)
        PKG_FILE_LST=$(comm -23 <(sort $RN.bak) <(sort $RN))
        for L in $PKG_FILE_LST; do
            echo $L >> $TMP_FILE
        done
        PKG_FILE_LST=$(tac $TMP_FILE)

        for L in $PKG_FILE_LST; do
            if [ -L $ROOTDIR/$L ]; then unlink $ROOTDIR/$L
            elif [ -f $ROOTDIR/$L ]; then rm -f $ROOTDIR/$L
            elif [ "$L" = "/" ]; then continue
            elif [ -d $ROOTDIR/$L ]; then find $ROOTDIR/$L -maxdepth 0 -type d -empty -delete
            fi
        done

        rm $RN.bak $TMP_FILE

        package_setup P_UPD
        package_hooks
        unset PKG VER
    else
        printf_red "error" "${PKG}_${RCP_VER}_${PKG_EXT}: no such file"
    fi
}

chk_update_pkgs() {
    if [ -f "$ROOTDIR/$INF_FILE" ] && [ -f "$RCP_FILE" ]; then
        . $ROOTDIR/$INF_FILE; INF_VER=$VER; unset VER
        . $RCP_FILE; RCP_VER=$VER; unset VER

        VER=$(echo -e "$RCP_VER\n$INF_VER" | sort -V | tail -n1)

        if [ "$RCP_VER" != "$INF_VER" ]; then
            if [ "$RCP_VER" = "$VER" ]; then
                update_packages
            fi
        fi
    else
        if [ -z "$GRP_LST" ]; then
            printf_red "error" "$i: no such recipe"
        fi
    fi
}

if [ $# -eq 0 ]; then echo "try $(basename $0) <package>"; exit 1; fi

arguments=($@)

for i in ${!arguments[@]}; do
    case "${arguments[$i]}" in
        rootdir=*)
            ROOTDIR=${arguments[$i]#*=}
            unset arguments[$i]
            ;;
        all)
            GRP_LST+="$(find $PKG_RCP_DIR/ -maxdepth 3 -name RECIPE) "
            unset arguments[$i]
            ;;
        *)
            if [ -d $PKG_RCP_DIR/${arguments[$i]} ]; then
                GRP_LST+="$(find $PKG_RCP_DIR/${arguments[$i]} -maxdepth 2 -name RECIPE) "
                unset arguments[$i]
            fi
            ;;
    esac
done

if [ -n "$GRP_LST" ]; then
    GRP_LST=$(for i in $GRP_LST; do echo $i; done | sort)
    arguments+=('grp_pkgs')
fi

for i in ${arguments[@]}; do
    case "$i" in
        grp_pkgs)
            for _pkg in $GRP_LST; do
                INF_FILE=$PKG_INF_DIR/$(basename ${_pkg%/RECIPE})
                RCP_FILE=$_pkg
                chk_update_pkgs
            done
            ;;
        *)
            INF_FILE=$PKG_INF_DIR/$i
            RCP_FILE=$(find $PKG_RCP_DIR/ -maxdepth 2 -name $i)/RECIPE
            chk_update_pkgs
            ;;
    esac
done

