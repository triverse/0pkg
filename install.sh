#!/bin/sh

. lib/libpath.sh
. lib/lib0pkg.sh

arguments=$@
for i in ${arguments[@]}; do
    case "$i" in
        rootdir=*)
            ROOTDIR=${i#*=}
            ;;
    esac
done

if [ "$ROOTDIR" = "/" ]; then unset ROOTDIR; fi

if [ ! -d ${ROOTDIR}${ARC_PKG_DIR} ]; then
    mkdir -p ${ROOTDIR}${ARC_PKG_DIR}
fi

if [ ! -d ${ROOTDIR}${ARC_SRC_DIR} ]; then
    mkdir -p ${ROOTDIR}${ARC_SRC_DIR}
fi

if [ ! -d ${ROOTDIR}${PKG_INF_DIR} ]; then
    mkdir -p ${ROOTDIR}${PKG_INF_DIR}
fi

if [ ! -d ${ROOTDIR}${PKG_LST_DIR} ]; then
    mkdir -p ${ROOTDIR}${PKG_LST_DIR}
fi

if [ ! -d ${ROOTDIR}${PKG_RCP_DIR} ]; then
    mkdir -p ${ROOTDIR}${PKG_RCP_DIR}
fi

install -v -Dm644 cfg/bld.conf ${ROOTDIR}${cfgdir}/bld
install -v -Dm644 cfg/0pkg.conf ${ROOTDIR}${cfgdir}/0pkg
install -v -Dm644 lib/lib0pkg.sh ${ROOTDIR}${libdir}/lib0pkg

if [ ! -f ${ROOTDIR}${libdir}/libpath ]; then
    install -v -Dm644 lib/libpath.sh ${ROOTDIR}${libdir}/libpath
fi

for f in $(ls exe); do
    install -v -Dm755 exe/$f ${ROOTDIR}${bindir}/${f//.sh}
done

install -v -d ${ROOTDIR}${datdir}/0pkg/hooks
install -v -t ${ROOTDIR}${datdir}/0pkg/proto -Dm755 dat/*_*
install -v -Dm644 dat/RECIPE ${ROOTDIR}${datdir}/0pkg/proto/RECIPE

sed -i -e "s,cfg/0pkg.conf,$cfgdir/0pkg,g" ${ROOTDIR}${libdir}/lib0pkg
sed -i -e "s,lib/libpath.sh,$libdir/libpath,g" ${ROOTDIR}${libdir}/lib0pkg
sed -i -e "s,lib/libpath.sh,$libdir/libpath,g" ${ROOTDIR}${bindir}/*0

