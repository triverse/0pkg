. cfg/0pkg.conf
. lib/libpath.sh

PKG_INF_DIR=$libdir/inf0
PKG_LST_DIR=$libdir/lst0
PKG_RUN_DIR=$libdir/run0
PKG_RCP_DIR=$libdir/rcp0
ARC_PKG_DIR=$vardir/arc/pkg
ARC_SRC_DIR=$vardir/arc/src

PKG_DIR=$blddir/pkg
SRC_DIR=$blddir/src

printf_green() {
    printf "\033[32;1m  $1 \033[0m $2\n" 
}

printf_red() {
    printf "\033[31;1m  $1 \033[0m $2\n"
}

backup_files() {
    for f in "$@"; do
        if [ -f "$ROOTDIR/$f" ]; then
            cp $ROOTDIR/$f $ROOTDIR/${f}.0pkg
        fi
    done
}

bld_remove_files() {
    rm -f $DAT_DIR/info/dir
    rm -f $LIB_DIR/charset.alias
}

bld_insert_files() {
    for f in P_ADD A_DEL P_DEL A_UPD P_UPD; do
        if [ -f $RCP_DIR/$f ]; then
            install -Dm755 $RCP_DIR/$f $PKG_DIR/$PKG_RUN_DIR/$PKG/$f
        fi
    done
}

package_setup() {
    if [ "$ROOTDIR" != "/" ]; then
        if [ -f $ROOTDIR/$PKG_RUN_DIR/$PKG/$1 ]; then
            chroot $ROOTDIR /execute/sh -c "$PKG_RUN_DIR/$PKG/$1"
        fi
    else
        if [ -f $PKG_RUN_DIR/$PKG/$1 ]; then
            $PKG_RUN_DIR/$PKG/$1
        fi
    fi
}

package_hooks() {
    if [ -d $ROOTDIR/$datdir/0pkg/hooks ]; then
        for hook in $ROOTDIR/$datdir/0pkg/hooks/*; do
            if [ -f $hook ]; then
                . $hook
                PKG_LST=""
                if [ -f $ROOTDIR/$PKG_LST_DIR/$PKG ]; then
                    PKG_LST=$(grep "$TARGET" $ROOTDIR/$PKG_LST_DIR/$PKG)
                fi
                if [ ! -z "$PKG_LST" ]; then
                    if [ "$ROOTDIR" != "/" ]; then
                        chroot $ROOTDIR /execute/sh -c "$RUNCMD"
                    else
                        $RUNCMD
                    fi
                fi
            fi
        done
    fi
}
